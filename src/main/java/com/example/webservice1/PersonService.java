package com.example.webservice1;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.*;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PersonService {
    WebClient.Builder webClient;

    Map<String, Person> persons = new HashMap<>();

    public Person getPersonById(String id) {
        return persons.get(id);
    }

    public Stream<Person> getAllPersons() {
        return persons.values().stream();
    }

    public Person createPerson(String firstname, String lastname) {
        Person person = new Person(UUID.randomUUID().toString(), firstname, lastname, new ArrayList<>());
        return persons.put(person.getId(), person);
    }

    public Person updatePerson(String id, String firstname, String lastname) {
        Person person = persons.get(id);
        person.setFirstname(firstname);
        person.setLastname(lastname);
        return person;
    }

    public String deletePersonById(String id) throws PersonNotFoundException {
        boolean b = persons.containsKey(id);
        if (!b) throw new PersonNotFoundException();
        webClient.build()
                .delete()
                .uri("http://localhost:8083/groups/delete-person-by-id/" + id)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
        persons.remove(id);
        return id;
    }

    public Person addGroupPerson(String personId, String groupId) {
        Person person = persons.get(personId);

        Group groupToSave = webClient.build()
                .post()
                .uri("http://localhost:8083/groups/addPersonToGrup/" + groupId + "?personId=" + person.getId())
                .retrieve()
                .bodyToMono(Group.class)
                .block();

        person.addGroup(groupToSave);
        return person;
    }

    public Group createGroup(String groupName) {
        Group group = webClient
                .build()
                .post()
                .uri("http://localhost:8083/groups/create/?groupName=" + groupName)
                .retrieve()
                .bodyToMono(Group.class)
                .block();


        return group;
    }

    public List<Group> getAllGroups() {
        List<Group> groups = webClient
                .build()
                .get()
                .uri("http://localhost:8083/groups/get-all/")
                .retrieve()
                .bodyToFlux(Group.class)
                .collectList()
                .block();
        return groups;
    }

    public Group updateGroup(String id, String groupName) {
        Group groups = webClient
                .build()
                .put()
                .uri("http://localhost:8083/groups/update/" + id + "?groupName=" + groupName)
                .retrieve()
                .bodyToMono(Group.class)
                .block();

        return groups;
    }

    public String deleteGroupById(String id) {
        String groups = webClient
                .build()
                .delete()
                .uri("http://localhost:8083/groups/delete-group-by-id/" + id)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        return groups;
    }

    public String getGroupName(String id) {
        String groups = webClient
                .build()
                .get()
                .uri("http://localhost:8083/groups/getName/" + id)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        return groups;
    }
}
