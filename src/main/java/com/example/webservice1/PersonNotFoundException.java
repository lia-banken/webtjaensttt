package com.example.webservice1;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "PERSON_NOT_FOUND")
public class PersonNotFoundException extends Exception {
}
